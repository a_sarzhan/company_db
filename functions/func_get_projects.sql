create or replace function get_projects (
	dep_name varchar,
	location_id int,
	separator character
) 
returns text
language plpgsql
as $$
declare 
    var_r record;
	projects text;
begin
	for var_r in(
			select p.project_name
			from project p
			inner join location l
			on p.location_id = l.location_id
			where l.department = $1
			and p.location_id = $2
    ) loop
			projects := CONCAT(projects, var_r.project_name, $3);
	end loop;
	return projects;
end; $$ 

select get_projects('KZ_DEP_DEV_APP_0003', 103, ',');