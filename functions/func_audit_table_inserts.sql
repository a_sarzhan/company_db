CREATE OR REPLACE FUNCTION audit_inserted_tables()
RETURNS trigger AS $body$
DECLARE
	_key text;
	_value text;
	description text;
BEGIN
	IF (TG_OP = 'INSERT') THEN
		FOR _key, _value IN
			SELECT n.key, n.value
			FROM each(hstore(NEW)) n
			WHERE n.value IS NOT NULL
		LOOP
			description := CONCAT(description, _key, '-', _value, ';');
		END LOOP;
		
		INSERT INTO public.logging
		VALUES (now(), TG_TABLE_NAME, description);
		RETURN NEW;
	END IF;
   
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER
SET search_path = public;
