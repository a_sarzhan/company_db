WITH cte_get_tablespace
AS (
SELECT t.schemaname, t.tablename, 
       COALESCE(t.tablespace, ts.default_tablespace) AS tablespace
FROM pg_tables t
   CROSS JOIN (
     SELECT tt.spcname AS default_tablespace
     FROM pg_database d
     JOIN pg_tablespace tt ON tt.oid = d.dattablespace
   ) ts
),
cte_size_column
AS (
	SELECT pg_size_pretty(SUM(pg_column_size(quote_ident(ic.column_name)))) AS total_column_size, cg.tablespace,
	SUM(pg_column_size(quote_ident(ic.column_name))) AS column_size
 	FROM information_schema.columns ic
	INNER JOIN cte_get_tablespace cg
	ON cg.tablename = ic.table_name
	GROUP BY cg.tablespace
),
cte_size_tablespace
AS
(
	SELECT spcname AS tablespace_name, 
    pg_size_pretty (pg_tablespace_size (quote_ident(spcname))) AS tablespace_size, 
	pg_tablespace_size (quote_ident(spcname)) AS space_size
	FROM pg_tablespace
)

SELECT  ct.tablespace_name, ct.tablespace_size, cz.total_column_size, 
		ROUND(cz.column_size * 100.0 / ct.space_size, 2) AS column_percentage
FROM cte_size_tablespace ct, cte_size_column cz
WHERE ct.tablespace_name = cz.tablespace

