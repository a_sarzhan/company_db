SELECT table_name, pg_size_pretty(pg_relation_size(quote_ident(table_name))) AS relation_size, 
pg_size_pretty(pg_total_relation_size(quote_ident(table_name))) AS total_size
FROM information_schema.tables
WHERE table_schema = 'public'
ORDER BY 1