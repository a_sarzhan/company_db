CREATE TRIGGER t_if_inserted_employee 
AFTER INSERT ON public.employee 
FOR EACH ROW EXECUTE PROCEDURE public.audit_inserted_tables();

CREATE TRIGGER t_if_inserted_emp_address 
AFTER INSERT ON public.employee_address 
FOR EACH ROW EXECUTE PROCEDURE public.audit_inserted_tables();

CREATE TRIGGER t_if_inserted_location 
AFTER INSERT ON public.location 
FOR EACH ROW EXECUTE PROCEDURE public.audit_inserted_tables();

CREATE TRIGGER t_if_inserted_project
AFTER INSERT ON public.project 
FOR EACH ROW EXECUTE PROCEDURE public.audit_inserted_tables();

CREATE TRIGGER t_if_inserted_project_load
AFTER INSERT ON public.project_workload
FOR EACH ROW EXECUTE PROCEDURE public.audit_inserted_tables();

CREATE TRIGGER t_if_inserted_department
AFTER INSERT ON public.department
FOR EACH ROW EXECUTE PROCEDURE public.audit_inserted_tables();