CREATE VIEW department_state_view
AS
WITH cte_emp
AS
(
	SELECT q1.department_name, COUNT(e.employee_id) AS employee_amount, q1.manager_name
	FROM 
		(SELECT d.department_name, CONCAT(e.first_name, ' ', e.last_name) AS manager_name
		FROM department d
		LEFT JOIN employee e
		ON d.manager_id = e.employee_id) q1
	LEFT JOIN employee e
	ON q1.department_name = e.department
	GROUP BY q1.department_name, q1.manager_name
),
cte_ratio
AS
(
	SELECT department, CAST(SUM(CASE WHEN gender='male' THEN 1 ELSE 0 END) AS FLOAT)/CAST(count(*) AS FLOAT) AS male_ratio, 
	CAST(SUM(CASE WHEN gender='female' THEN 1 ELSE 0 END) AS FLOAT)/CAST(count(*) AS FLOAT) AS female_ratio 
	FROM employee
	GROUP BY department
),
cte_dep
AS 
(
	SELECT l.department, COUNT(p.project_name) AS project_amount
	FROM location l
	INNER JOIN project p
	ON l.location_id = p.location_id
	GROUP BY l.department
),
cte_project_hours
AS
(
	SELECT l.department, pr.project_name, SUM(pw.work_hours_week) AS total_hours
	FROM location l
	INNER JOIN project pr ON l.location_id = pr.location_id
	LEFT JOIN project_workload pw ON pr.project_name = pw.project_name
	GROUP BY l.department, pr.project_name
),
cte_max
AS
(
	SELECT c1.department, c1.project_name AS max_hours_project, c1.total_hours AS max_hours
	FROM cte_project_hours c1
	WHERE c1.total_hours = 
		(SELECT MAX(c2.total_hours)
		FROM cte_project_hours c2
		WHERE c2.department = c1.department
		GROUP BY department)
),
cte_min
AS
(
	SELECT c1.department, c1.project_name AS min_hours_project, c1.total_hours AS min_hours
	FROM cte_project_hours c1
	WHERE c1.total_hours = 
		(SELECT MIN(c2.total_hours)
		FROM cte_project_hours c2
		WHERE c2.department = c1.department
		GROUP BY department)
)

SELECT ce.department_name, ce.employee_amount, ce.manager_name, cr.male_ratio, cr.female_ratio, cd.project_amount, 
cm.max_hours_project, cx.min_hours_project
FROM cte_emp ce
LEFT JOIN cte_ratio cr
ON ce.department_name = cr.department
LEFT JOIN cte_dep cd
ON ce.department_name = cd.department
LEFT JOIN cte_max cm
ON ce.department_name = cm.department
LEFT JOIN cte_min cx
ON ce.department_name = cx.department
ORDER BY ce.department_name;
