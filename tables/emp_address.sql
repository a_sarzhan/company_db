-- Table: public.employee_address

-- DROP TABLE public.employee_address;

CREATE TABLE public.employee_address
(
    address_id integer NOT NULL,
    country character varying(30) NOT NULL,
    state character varying(30),
    city character varying(30) NOT NULL,
    street character varying(30) NOT NULL,
    apartment_no integer,
    flat_no integer,
    employee_id integer,
    CONSTRAINT employee_address_pkey PRIMARY KEY (address_id),
    CONSTRAINT employee_id_fkey FOREIGN KEY (employee_id)
        REFERENCES public.employee (employee_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE public.employee_address
    OWNER to postgres;

-- Trigger: t_if_inserted_emp_address

-- DROP TRIGGER t_if_inserted_emp_address ON public.employee_address;

CREATE TRIGGER t_if_inserted_emp_address
    AFTER INSERT
    ON public.employee_address
    FOR EACH ROW
    EXECUTE PROCEDURE public.audit_inserted_tables();