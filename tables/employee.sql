-- Table: public.employee

-- DROP TABLE public.employee;

CREATE TABLE public.employee
(
    employee_id integer NOT NULL,
    first_name character varying(15) NOT NULL,
    last_name character varying(30) NOT NULL,
    birth_date date NOT NULL,
    gender character varying(10) NOT NULL,
    salary numeric,
    supervisor_id integer NOT NULL,
    department character varying(50) NOT NULL,
    CONSTRAINT employee_pkey PRIMARY KEY (employee_id),
    CONSTRAINT employee_department_fkey FOREIGN KEY (department)
        REFERENCES public.department (department_name) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION,
    CONSTRAINT employee_supervisor_id_fkey FOREIGN KEY (supervisor_id)
        REFERENCES public.employee (employee_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION,
    CONSTRAINT supervisor_validation CHECK (supervisor_id <> employee_id)
)

TABLESPACE pg_default;

ALTER TABLE public.employee
    OWNER to postgres;

-- Trigger: t_if_inserted_employee

-- DROP TRIGGER t_if_inserted_employee ON public.employee;

CREATE TRIGGER t_if_inserted_employee
    AFTER INSERT
    ON public.employee
    FOR EACH ROW
    EXECUTE PROCEDURE public.audit_inserted_tables();