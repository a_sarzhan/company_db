-- Table: public.project

-- DROP TABLE public.project;

CREATE TABLE public.project
(
    project_name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    project_no integer NOT NULL,
    location_id integer NOT NULL,
    CONSTRAINT project_pkey PRIMARY KEY (project_name),
    CONSTRAINT project_location_id_fkey FOREIGN KEY (location_id)
        REFERENCES public.location (location_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.project
    OWNER to postgres;

-- Trigger: t_if_inserted_project

-- DROP TRIGGER t_if_inserted_project ON public.project;

CREATE TRIGGER t_if_inserted_project
    AFTER INSERT
    ON public.project
    FOR EACH ROW
    EXECUTE PROCEDURE public.audit_inserted_tables();