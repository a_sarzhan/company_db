-- Table: public.department

-- DROP TABLE public.department;

CREATE TABLE public.department
(
    department_name character varying(50) NOT NULL,
    manager_id integer,
    CONSTRAINT department_pkey PRIMARY KEY (department_name),
    CONSTRAINT department_manager_id_fkey FOREIGN KEY (manager_id)
        REFERENCES public.employee (employee_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.department
    OWNER to postgres;

-- Trigger: t_if_inserted_department

-- DROP TRIGGER t_if_inserted_department ON public.department;

CREATE TRIGGER t_if_inserted_department
    AFTER INSERT
    ON public.department
    FOR EACH ROW
    EXECUTE PROCEDURE public.audit_inserted_tables();