CREATE TABLE public.department_location
(
    dep_location_id integer NOT NULL,
    dep_name character varying(30) NOT NULL,
    location_id integer NOT NULL,
    CONSTRAINT department_location_pkey PRIMARY KEY (dep_location_id),
    CONSTRAINT department_location_dep_name_fkey FOREIGN KEY (dep_name)
        REFERENCES public.department (dep_name) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT department_location_location_id_fkey FOREIGN KEY (location_id)
        REFERENCES public.location (location_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE public.department_location
    OWNER to postgres;