-- Table: public.project_workload

-- DROP TABLE public.project_workload;

CREATE TABLE public.project_workload
(
    project_load_id integer NOT NULL,
    project_name character varying(30) NOT NULL,
    employee_id integer NOT NULL,
    work_hours_week double precision,
    CONSTRAINT project_workload_pkey PRIMARY KEY (project_load_id),
    CONSTRAINT project_workload_employee_id_fkey FOREIGN KEY (employee_id)
        REFERENCES public.employee (employee_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION,
    CONSTRAINT project_workload_project_name_fkey FOREIGN KEY (project_name)
        REFERENCES public.project (project_name) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.project_workload
    OWNER to postgres;

-- Trigger: t_if_inserted_project_load

-- DROP TRIGGER t_if_inserted_project_load ON public.project_workload;

CREATE TRIGGER t_if_inserted_project_load
    AFTER INSERT
    ON public.project_workload
    FOR EACH ROW
    EXECUTE PROCEDURE public.audit_inserted_tables();