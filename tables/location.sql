-- Table: public.location

-- DROP TABLE public.location;

CREATE TABLE public.location
(
    location_id integer NOT NULL,
    country character varying(30) NOT NULL,
    state_prov character varying(30),
    city character varying(30),
    street character varying(30),
    apartment_no integer,
    zip_code character varying(10),
    department character varying(50),
    CONSTRAINT location_pkey PRIMARY KEY (location_id),
    CONSTRAINT location_department_fkey FOREIGN KEY (department)
        REFERENCES public.department (department_name) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.location
    OWNER to postgres;

-- Trigger: t_if_inserted_location

-- DROP TRIGGER t_if_inserted_location ON public.location;

CREATE TRIGGER t_if_inserted_location
    AFTER INSERT
    ON public.location
    FOR EACH ROW
    EXECUTE PROCEDURE public.audit_inserted_tables();