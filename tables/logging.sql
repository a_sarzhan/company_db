-- Table: public.logging

-- DROP TABLE public.logging;

CREATE TABLE public.logging
(
    insert_date timestamp without time zone,
    ref_table character varying(30),
    description character varying
)

TABLESPACE pg_default;

ALTER TABLE public.logging
    OWNER to postgres;